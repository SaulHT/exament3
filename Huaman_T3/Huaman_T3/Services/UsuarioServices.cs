﻿using Huaman_T3.DB;
using Huaman_T3.Models;
using Huaman_T3.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Services
{
    public class UsuarioServices : IusuarioRepositorio
    {
        private AppExameContex db;

        public UsuarioServices(AppExameContex db)
        {
            this.db = db;
        }
        public Usuario getUsuario(string nombre, string contraseña)
        {
            var user=db.usuarios.Where(o => o.nombre == nombre && o.contraseña == contraseña).FirstOrDefault();

            return user;
        }
    }
}
