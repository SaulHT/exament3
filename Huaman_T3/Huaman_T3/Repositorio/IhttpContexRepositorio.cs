﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Huaman_T3.Repositorio
{
   public  interface IhttpContexRepositorio
    {
        public void setHttpContext(HttpContext context);
        public void login(ClaimsPrincipal claims);
    }
}
