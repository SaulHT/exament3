﻿using Huaman_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Repositorio
{
   public  interface IHistoriaRepositorio
    {
        List<Historia> lista();
        List<Especie> listaEspecie();
        List<Raza> listaRaza();

       bool siexist(Historia historia);
        void crearNuevoHistoria(Historia historia);
    }
}
