﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Models
{
    public class Historia
    {
        public int id { get; set; }
        public string codigo { get; set; }
        public DateTime fecha { get; set; }
        public string nombre { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string sexo { get; set; }
        public string tamaño { get; set; }
        public string datosParticulare { get; set; }
        public string nombreDueño { get; set; }
        public string direccionDueño { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public int idRaza { get; set; }
        public int idEspecie { get; set; }

        public Raza Raza { get; set; }
        public Especie Especie { get; set; }
    }
}
