﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Huaman_T3.DB;
using Huaman_T3.Repositorio;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Huaman_T3.Controllers
{
    public class AuthController : Controller
    {
        private readonly AppExameContex db;
        private IusuarioRepositorio Iusuario;
        private IhttpContexRepositorio IHttp;
        public AuthController(AppExameContex db, IusuarioRepositorio Iusuario, IhttpContexRepositorio IHttp)
        {
            this.db = db;
            this.Iusuario = Iusuario;
            this.IHttp = IHttp;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string nombre,string contraseña)
        {
            // var user = db.usuarios.Where(o=>o.nombre==nombre && o.contraseña==contraseña).FirstOrDefault();
            var user = Iusuario.getUsuario(nombre, contraseña);

            if (user !=null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,nombre)
                };

                var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var Principal = new ClaimsPrincipal(userIdentity);

               // HttpContext.SignInAsync(Principal);

                IHttp.setHttpContext(HttpContext);
                IHttp.login(Principal);

                return RedirectToAction("Index","HistoriaClinica");
            }

            return View();
        }

        public IActionResult Logaout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
