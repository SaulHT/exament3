﻿using Huaman_T3.DB.Maps;
using Huaman_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.DB
{
    public class AppExameContex:DbContext
    {
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Raza> razas { get; set; }
        public DbSet<Historia> historias { get; set; }
        public DbSet<Especie>especies  { get; set; }

        public AppExameContex(DbContextOptions<AppExameContex> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new MapsEspecie());
            modelBuilder.ApplyConfiguration(new MapsHistoria());
            modelBuilder.ApplyConfiguration(new MapsRaza());
            modelBuilder.ApplyConfiguration(new MapsUsuario());
        }
    }
}
