﻿using Huaman_T3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.DB.Maps
{
    public class MapsHistoria : IEntityTypeConfiguration<Historia>
    {
        public void Configure(EntityTypeBuilder<Historia> builder)
        {
            builder.ToTable("Historia");
            builder.HasKey(o=>o.id);

            builder.HasOne(o => o.Especie).WithMany(o => o.historia).HasForeignKey(o=>o.idEspecie);
            builder.HasOne(o => o.Raza).WithMany(o => o.historia).HasForeignKey(o=>o.idRaza);
        }
    }
}
