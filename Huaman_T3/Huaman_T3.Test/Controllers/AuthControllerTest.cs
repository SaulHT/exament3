﻿using Huaman_T3.Controllers;
using Huaman_T3.Models;
using Huaman_T3.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Huaman_T3.Test.Controllers
{
   
    public class AuthControllerTest
    {
        [Test]
        public void login()
        {
            var user = new Mock<IusuarioRepositorio>();
            var htt = new Mock<IhttpContexRepositorio>();

            user.Setup(o => o.getUsuario("josue", "123")).Returns(new Usuario { }); ;

            var controller = new AuthController(null, user.Object, htt.Object);
            var view = controller.Login("josue", "123");

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
    }
}
