﻿using Huaman_T3.Controllers;
using Huaman_T3.Models;
using Huaman_T3.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Huaman_T3.Test.Controllers
{
    public class HistoriaClinicaTest
    {
        [Test]
        public void index()
        {
            var list = new Mock<IHistoriaRepositorio>();

            list.Setup(o=>o.lista()).Returns(new List<Historia>());

            var control = new HistoriaClinicaController(null, list.Object);
            var view = control.Index();

            Assert.IsInstanceOf<ViewResult>(view);
        }

     [Test]
        public void CrearNuevo()
        {
            var historia = new Historia()
            {
                codigo = "005",
                fecha=DateTime.Now,
                nombre="toby",
               fechaNacimiento=new DateTime(2020,1,5),
                sexo="macho",
                idEspecie=1,
                idRaza=1,
                tamaño="mediano",
                datosParticulare="es de color cafe con blanco ",
                nombreDueño="Roger",
                direccionDueño="los pinos #520",
                telefono="960998211",
                email="Roger_20@gmail.com"
            };

            var nuevo = new Mock<IHistoriaRepositorio>();
            nuevo.Setup(o => o.crearNuevoHistoria(historia));

            var control = new HistoriaClinicaController(null, nuevo.Object);
            var resul = control.Crear(historia);

            Assert.IsInstanceOf<RedirectToActionResult>(resul);
        }
    }
}
